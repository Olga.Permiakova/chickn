
# Test environments

* `Ubuntu 18.04.5`: release
* Rhub `Debian`: release, devel
* Rhub `Windows`: release, devel

# R CMD check --as-cran
0 ERRORS | 0 WARNINGS | 1 NOTES

* NOTE: checking CRAN incoming feasibility (13.5s)
          Maintainer: ‘Olga Permiakova <olga.permiakova@gmail.com>’
          New submission

# CRAN re-submission (chickn version 1.2.3.)
  * checking examples ... ERROR
    Running examples in ‘chickn-Ex.R’ failed
    The error most likely occurred in: 
    Name: Sketch
    Title: Sketch
    Aliases: Sketch
    Error in split_foreach(p.FUN, ind, X, ..., .combine = p.combine, ncores = ncores) :
      Package is not compatible with SunOS
    Calls: Sketch -> big_parallelize -> split_foreach
    Execution halted
    
    checking examples ... ERROR
    Running examples in ‘chickn-Ex.R’ failed
    The error most likely occurred in: 
    Name: Sketch
    Title: Sketch
    Aliases: Sketch
    Error in split_foreach(p.FUN, ind, X, ..., .combine = p.combine, ncores = ncores) :
    Package is not compatible with Darwin
    Calls: Sketch -> big_parallelize -> split_foreach
    
> The package is now compatible with Mac OS (Darwin) and Solaris OS (SunOS) 


# CRAN re-submission (chickn version 1.2.1.)
* Please write TRUE and FALSE instead of T and F. (Please don't use 'T' or 'F' as vector names.)

> "T" and "F" have been replaced with "TRUE" and "FALSE" respectively. I checked that "T" or "F" were not used as vector names.

# CRAN re-submission (chickn version 1.2.)
* If there are references describing the methods in your package, please
add these in the description field of your DESCRIPTION file in the form
authors (year) <doi:...>
authors (year) <arXiv:...>
authors (year, ISBN:...)
or if those are not available: <https:...>
with no space after 'doi:', 'arXiv:', 'https:' and angle brackets for
auto-linking.
(If you want to add a title as well please put it in quotes: "Title")

> The references describing the methods in the package were added in the description field of DESCRIPTION file.    

* `\dontrun{}` should only be used if the example really cannot be executed
(e.g. because of missing additional software, missing API keys, ...) by
the user. That's why wrapping examples in \dontrun{} adds the comment
("# Not run:") as a warning for the user.
Does not seem necessary.
Please unwrap the examples if they are executable in < 5 sec, or replace
`\dontrun{}` with `\donttest{}`.

> Examples that take more then 5s were encompased by `donttest` instead of `dontrun`.

* You write information messages to the console that cannot be easily
suppressed.
It is more R like to generate objects that can be used to extract the
information a user is interested in, and then print() that object.
Instead of print()/cat() rather use message()/warning()  or
if(verbose)cat(..) (or maybe stop()) if you really have to write text to
the console.
(except for print, summary, interactive functions)

> `if(verbose) message()` was added to allow a user dysplaying the process steps on the console (by default verbose = FALSE).  

# CRAN submission of chickn 1.1. (bug fixes)

## NOTES
* Flavor: r-devel-linux-x86_64-debian-gcc, r-devel-windows-ix86+x86_64
  Check: CRAN incoming feasibility, Result: NOTE
  Maintainer: 'Olga Permiakova <olga.permiakova@gmail.com>'

  New submission

  Possibly mis-spelled words in DESCRIPTION:
    Chromatogram (3:8)
    Compressive (3:34)
    HIerarchical (3:21)
    Nystrom (3:59)
    chromatographic (9:115)
    compressive (9:56)
> To avoid this note, the package description has been rewritten. The word 'HIerarchical' has been replaced by 'Hierarchical', 'Compressive' by 'Compressed', the rest of the words have been removed. 

*  Flavor: r-devel-linux-x86_64-debian-gcc, r-devel-windows-ix86+x86_64
    Found the following (possibly) invalid URLs:
    URL: http://arxiv.org/abs/1606.02838 (moved to https://arxiv.org/abs/1606.02838)
      From: man/CHICKN_W1.Rd
            man/DrawFreq.Rd
            man/EstimSigma.Rd
            man/GenerateFrequencies.Rd
            man/Sketch.Rd
      Status: 200
      Message: OK
      
> The URL [http://arxiv.org/abs/1606.02838] has been removed from REFERENCES.bib. The corresponding reference has been replaced with a new (printed version of the article).

* Flavor: r-devel-linux-x86_64-debian-gcc
  Check: for non-standard things in the check directory, Result: NOTE
  Found the following files/directories:
    'A_cumsum.bk' 'A_cumsum.rds' 'C.bk' 'C.rds' 'Centroids_out.bk'
    'Centroids_out.rds' 'Cluster_assign_out.bk' 'Cluster_assign_out.rds'
    'NystromKernel.bk' 'NystromKernel.rds' 'set_c.RData'
    
> All examples now use `tempfile()` as the output directory instead of `getwd()`. 

## WARNING
   * Flavor: r-devel-linux-x86_64-debian-gcc
Check: for code/documentation mismatches, Result: WARNING
  Variables with usage in documentation object 'UPS2' but not in code:
    'UPS2' 
  
> "UPS2" is a dataset included in the package. It is now used in examples. 

## ERROR
* Flavor: r-devel-windows-ix86+x86_64
Check: whether package can be installed, Result: ERROR
  Installation failed.
  
> The `Makevars` and `Makevars.win` files have been added to make the package compatible with Windows. Now in parallel computing, the `makeCluster()` function uses the `PSOCK` cluster type on Windows and the `FORK` cluster type on Linux.
