# chickn version 1.2.3.
## Minor changes
* The package description was simplified.
* The package is now compatible with Soloaris OS (SunOS) and Mac OS (Darwin).
# chickn version 1.2.2
* The date is updated in DESCRIPTION 
# chickn version 1.2.1.
## Minor changes
* "T" and "F" have been replaced with "TRUE" and "FALSE" respectively. 
# chickn version 1.2
## Minor changes
* `if(verbose) message()` was added to allow an user dysplaying the process steps on the console (by default verbose = FALSE).  

# chickn version 1.1
## Major changes
  * The `Makevars` and `Makevars.win` files have been added to make the package compatible with Windows.
  * `makeCluster()` now uses `PSOCK` cluster type under Windows and `FORK` cluster type under Linux.
## Minor changes
  * In all functions, the default for the `DIR_ouptup` argument is now `tempfile()`.
  * An additional `DIR_save` argument has been added to `Nystrom_kernel_parallel` function. Now it should be used to specify the directory in which the resulting Nystrom FBM should be saved. `DIR_output` is now only used for intermediate files.
  * The documentation has been rewritten.



